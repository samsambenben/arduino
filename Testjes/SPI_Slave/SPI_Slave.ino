/* SPI Slave - Hello World (from Nick Gammon's sample)

Arduino Mega ADK is acting as an SPI Slave while 
a corresponding Arduino Duemilanove is acting as the
SPI Master

Circuit:
SS_PIN: pin 53
MISO: pin 50
MOSI: pin 51
SCK: pin 52

*/

#include <stdlib.h>
#include <SPI.h>

long int count = 1;
uint8_t buffer[3];
char buf [100];
volatile byte pos;
volatile boolean process_it;
#define SCK_PIN  13
#define MISO_PIN 12
#define MOSI_PIN 11
#define SS_PIN   10  

void setup (void)
{
  Serial.begin (115200);   // debugging

  // have to send on master in, *slave out*
  pinMode(MOSI_PIN, INPUT);
  pinMode(MISO_PIN, OUTPUT);
  pinMode(SCK_PIN, INPUT);
  pinMode(SS_PIN, INPUT);

  //SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  
  DDRB=(1<<DDB4);  //prev
  SPCR=(1<<SPE);   //prev
  
  // turn on SPI in slave mode
  SPCR |= _BV(SPE);
    
  // turn on interrupts
  SPCR != _BV(SPIE);
  
  // get ready for an interrupt 
  pos = 0;   // buffer empty
  process_it = false;

  // now turn on interrupts
  SPI.attachInterrupt();
  
  // interrupt for SS falling edge
  //attachInterrupt (0, ss_falling, FALLING);

}  // end of setup


// SPI interrupt routine
ISR (SPI_STC_vect)
{
byte c = SPDR;  // grab byte from SPI Data Register
  
  // add to buffer if room
  if (pos < sizeof buf)
    {
    buf [pos++] = c;
    SPDR = c; // Added response - return same string
    
    // example: newline means time to process buffer
    if (c == '\n')
      process_it = true;
      
    }  // end of room available
}  // end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  char c;
  
  if (process_it)
    {
    buf [pos] = 0;  
    Serial.println (buf);
    pos = 0;
    process_it = false;
    
    // Send response test string to Master
    //for (const char * p = "Goodbye, earth!\n" ; c = *p; p++)
    //  SPDR=c; //SPI.transfer(c);
    
    }  // end of flag set
    
}  // end of loop



