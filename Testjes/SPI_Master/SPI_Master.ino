/* SPI Master - Hello World (derived from Nick Gammon's samples)

Arduino Mega is acting as an SPI Master for 
a corresponding Arduino Mega board

Circuit:
SS_PIN: pin 53
MISO: pin 50
MOSI: pin 51
SCK: pin 52

*/

#include <stdlib.h>
#include <SPI.h>

long int count = 1;
byte config = B01010000;
#define SCK_PIN  13
#define MISO_PIN 12
#define MOSI_PIN 11
#define SS_PIN   10
uint8_t buffer[128];
char respBuffer[100];
volatile byte pos;
volatile boolean process_it;

void setup()
{
  byte tmp;
  
  // Put SCK, MOSI, SS pins into output mode
  // also put SCK, MOSI into LOW state, and SS into HIGH state.
  // Then put SPI hardware into Master mode and turn SPI on
  pinMode(MOSI_PIN, OUTPUT);
  pinMode(MISO_PIN, INPUT);
  pinMode(SCK_PIN, OUTPUT);
  pinMode(SS_PIN, OUTPUT);
  
  digitalWrite(SS_PIN, HIGH);  // Ensure SS stays high for now
  
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1);
  byte clr;
  clr=SPSR;
  clr=SPDR;
  
  Serial.begin(115200);
}


byte transferAndWait(const byte what)
{
  byte a = SPI.transfer(what);
  delayMicroseconds(20);
  return a;
} // end of transferAndWait

void loop()
{
char c;
byte r;

// enable Slave Select  
digitalWrite(SS_PIN, LOW);
   
pos=0;
// Send test string
for (const char *p="Hello, world!\n";c=*p;p++)
{
   //SPI.transfer(c);
   r=transferAndWait(c);
   respBuffer[pos++]=r;
   Serial.print(c);
   Serial.print(r);
}

//respBuffer[pos]=0;
Serial.println("RX:");
Serial.println(pos,DEC);
Serial.println(respBuffer);
pos=0;
   

delay(100);

// Disable slave select
digitalWrite(SS_PIN, HIGH);

// 1 second delay
delay(100);
   
}




