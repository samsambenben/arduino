int PWM;
int i = 0;

unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

const int psh = 8;
const int led = 3;

int ledState = HIGH;         // the current state of the output pin
int pshState;             // the current reading from the input pin
int lastpshState = LOW;   // the previous reading from the input pin

void setup() {
  pinMode(led, OUTPUT);
  pinMode(psh, INPUT);
}

void loop() {
  // read the state of the switch into a local variable:
  int reading = digitalRead(psh);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != lastpshState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != pshState) {
      pshState = reading;

      // only toggle the LED if the new button state is HIGH
      if (pshState == HIGH) {
        i++;
      }
    }
  }
  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  lastpshState = reading;
  
  switch (i) {
    case 1:
      PWM = 0;
      break;
    case 2:
      PWM = 64;
      break;
    case 3:
      PWM = 127;
      break;
    case 4:
      PWM = 191;
      break;
    case 5:
      PWM = 255;
      i = 0;
      break;
    delay (100);
  } 
  analogWrite (led, PWM);
 }


