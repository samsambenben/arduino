int i = 0;

unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

const int psh = 2;
const int led = 13;

int ledState = HIGH;         // the current state of the output pin
int pshState;             // the current reading from the input pin
int lastpshState = LOW;   // the previous reading from the input pin
volatile byte state = LOW;

void setup () {
  pinMode(led, OUTPUT);
  pinMode(psh, INPUT);
  Serial.begin (9600);
  attachInterrupt(digitalPinToInterrupt(2), test_functie, HIGH);
}

void loop () {

  //Serial.println ("Test");
  //delay (1000);
  digitalWrite (led, state);
  Serial.println (analogRead (3));
  Serial.println (" ");

}

void test_functie () {
  state = !state;
}

