int i = 0;

unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

const int psh = 8;
const int led = 3;

int ledState = HIGH;         // the current state of the output pin
int pshState;             // the current reading from the input pin
int lastpshState = LOW;   // the previous reading from the input pin

void setup () {
  pinMode(led, OUTPUT);
  pinMode(psh, INPUT);  
}

void loop () {  
  int reading = digitalRead(psh);
  
  if (reading != lastpshState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != pshState) {
      pshState = reading;

      // only toggle the LED if the new button state is HIGH
      if (pshState == HIGH) {
        i++;
      }
    }
  }
  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  lastpshState = reading;
}
