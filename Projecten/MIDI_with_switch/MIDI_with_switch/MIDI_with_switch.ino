/*MIDI Commands:
 * 0x80 Note off
 * 0x90 Note on
 * 0xA0 Aftertouch
 * 0xB0 Continuous controller
 * 0xC0 Patch change
 * 0xD0 Channel pressure
 * 0xE0 Pitch bend
 * 0xF0 Non musical commands
 * 
 * Max pitch value = 127 = 0x7F
 * Max velocity value = 0x7F
 * 
 * 
 */

int MIDI_input1 = 3;
int MIDI_input2 = 4;
int potmeter = A0;
//int interrupPin = 2;
int note1 = 36; //36 = C1
int note2 = 38; //38 = D1
//int note;
boolean i;
int toestand;

void setup() {
  pinMode (MIDI_input1, INPUT);
  pinMode (MIDI_input2, INPUT);
  //pinMode (keybutton, INPUT);
  Serial.begin(9600);
  //attachInterrupt (interruptPin, 
}

void loop() {

    /*if (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
      MIDICommand (0x90, note1, 0x7F);       //sending 2 NoteOn message to CH1  
      MIDICommand (0x90, note2, 0x7F);
    while (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
      MIDICommand (0xA0, note1, 0x7F);       //sending 2 NoteOn message to CH1  
      MIDICommand (0xA0, note2, 0x7F);
      PitchWheelChange(map(analogRead(potmeter),0, 1023, -8000, 8000)); //map incomming potmeter
      }
    }*/
     
    if (digitalRead(MIDI_input1) == HIGH) {
      MIDICommand (0x90, note1, 0x7F);       //sending NoteOn message to CH1
    while (digitalRead(MIDI_input1) == HIGH) {
      MIDICommand (0xA0, note1, 0x7F);       //Sending Aftertouch message to CH1
      PitchWheelChange(map(analogRead(potmeter),0, 1023, -8000, 8000)); //map incomming potmeter
      if (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
        //MIDICommand (0x90, note1, 0x7F);       //sending 2 NoteOn message to CH1  
        MIDICommand (0x90, note2, 0x7F);
      while (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
        MIDICommand (0xA0, note1, 0x7F);       //sending 2 NoteOn message to CH1  
        MIDICommand (0xA0, note2, 0x7F);
        PitchWheelChange(map(analogRead(potmeter),0, 1023, -8000, 8000)); //map incomming potmeter
          }
        }
      } 
    }
    
    if (digitalRead(MIDI_input2) == HIGH) { 
    MIDICommand (0x90, note2, 0x7F);       //sending NoteOn message to CH1
    while (digitalRead(MIDI_input1) == HIGH) {
      MIDICommand (0xA0, note2, 0x7F);       //Sending Aftertouch message to CH1
      PitchWheelChange(map(analogRead(potmeter),0, 1023, -8000, 8000)); //map incomming potmeter  
      if (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
        MIDICommand (0x90, note1, 0x7F);       //sending 2 NoteOn message to CH1  
        //MIDICommand (0x90, note2, 0x7F);
      while (digitalRead(MIDI_input1) == HIGH && digitalRead(MIDI_input2) == HIGH) {
        MIDICommand (0xA0, note1, 0x7F);       //sending 2 NoteOn message to CH1  
        MIDICommand (0xA0, note2, 0x7F);
        PitchWheelChange(map(analogRead(potmeter),0, 1023, -8000, 8000)); //map incomming potmeter
          }
        }
      }  
    }
  MIDICommand (0x80, note1, 0x7F);         //Sending NoteOff message to CH1
  MIDICommand (0x80, note2, 0x7F);
}
 

void MIDICommand (int cmd, int pitch, int velocity) {   //Function for sending MIDICommands
  Serial.write(cmd);
  Serial.write(pitch);
  Serial.write(velocity);
}

void PitchWheelChange(int value) {                      //Function for pitchbend
    unsigned int change = 0x2000 + value;  //  0x2000 == No Change
    unsigned char low = change & 0x7F;  // Low 7 bits
    unsigned char high = (change >> 7) & 0x7F;  // High 7 bits

   MIDICommand(0xE0, low, high);
}


